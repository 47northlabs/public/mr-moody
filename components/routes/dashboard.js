var debug = require('debug')('botkit:oauth');
var mongoose = require('mongoose');
var mongo = require('mongodb');


module.exports = function(webserver, controller) {

    var handler = {
        dashboard: function(req, res) {
       
        }
    }

    debug('Configured /dashboard url');
    webserver.get('/dashboard', handler.dashboard);

    return handler;
}
