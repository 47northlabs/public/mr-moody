# Botkit Starter Kit for Slack Bots

This repo contains everything you need to get started building a Slack bot with [Botkit](https://botkit.ai) and [Botkit Studio](https://botkit.ai).

Botkit is designed to ease the process of designing and running useful, creative bots that live inside messaging platforms. Bots are applications that can send and receive messages, and in many cases, appear alongside their human counterparts as users.

Some bots talk like people, others silently work in the background, while others present interfaces much like modern mobile applications. Botkit gives developers the necessary tools for building bots of any kind! It provides an easy-to-understand interface for sending and receiving messages so that developers can focus on creating novel applications and experiences instead of dealing with API endpoints.

Our goal with Botkit is to make bot building easy, fun, and accessible to anyone with the desire to create a future filled with talking machines!

If you are looking to create a bot on other platforms using Glitch, check out the [Botkit project page](https://glitch.com/botkit).

### What's Included
* [Botkit core](https://botkit.ai/docs/core.html) - a complete programming system for building conversational software
* [Pre-configured Express.js webserver](https://expressjs.com/) including:
   * A customizable "Install my Bot" homepage
   * Login and oauth endpoints that allow teams to install your bot
   * Webhook endpoints for communicating with platforms
* Sample skill modules that demonstrate various features of Botkit
* A customizable onboarding experience for new teams powered by Botkit Studio

### Getting Started

There are a myriad of methods you can use to set up an application on Slack, here are some of your options:

#### Install Botkit

[Remix this project on Glitch](https://glitch.com/~botkit-slack)

[Deploy to Heroku](https://heroku.com/deploy?template=https://github.com/howdyai/botkit-starter-slack/master)

Clone this repository using Git:

`git clone https://github.com/howdyai/botkit-starter-slack.git`

Install dependencies, including [Botkit](https://github.com/howdyai/botkit):

```
cd botkit-starter-slack
npm install
```

#### Set up your Slack Application 
Once you have setup your Botkit development enviroment, the next thing you will want to do is set up a new Slack application via the [Slack developer portal](https://api.slack.com/). This is a multi-step process, but only takes a few minutes. 

* [Read this step-by-step guide](https://botkit.ai/docs/provisioning/slack-events-api.html) to make sure everything is set up. 

* We also have this [handy video walkthrough](https://youtu.be/us2zdf0vRz0) for setting up this project with Glitch.

Update the `.env` file with your newly acquired tokens.

Launch your bot application by typing:

`node .`

Now, visit your new bot's login page: http://localhost:3000/login

Now comes the fun part of [making your bot!](https://botkit.ai/docs/#build-your-bot)


### Extend This Starter kit

This starter kit is designed to provide developers a robust starting point for building a custom bot. Included in the code are a set of sample bot "skills" that illustrate various aspects of the Botkit SDK features.  Once you are familiar with how Botkit works, you may safely delete all of the files in the `skills/` subfolder.

Developers will build custom features as modules that live in the `skills/` folder. The main bot application will automatically include any files placed there.

A skill module should be in the format:

```
module.exports = function(controller) {

    // add event handlers to controller
    // such as hears handlers that match triggers defined in code
    // or controller.studio.before, validate, and after which tie into triggers
    // defined in the Botkit Studio UI.

}
```

### Getting Started

*	Clone the project: git clone https://gitlab.com/47northlabs/public/mr-moody.git
*	Navigate to the folder containing the project and run “npm install”
*	Create in the project folder .env file using e.g. touch .env
*	The contents of the .env file are as follows (three parameters need to be added which are):
	* clientId ()
	* clientSecret
	* token

When you create a new slack bot, you will find these params as follows:

* under "Basic Information", you will find the parameters Client ID and Client Secret.
* under "QAuth & Permissions", you will find a parameter called "Bot User OAuth Access Token". The value of this parameter will be the value of the token.


* To start the teh application, please type 'node .'


### Some quick notes 

-  to make a chain of questions, you have to conisder to put the second question inside the first one to make it come after each other. An
exampel of a set of questions is as follows:

 controller.hears('qq', 'message_received', function(bot, message) {
        bot.startConversation(message, function(err, convo) {
            convo.say('Lets get to know each other a little bit!')

            convo.ask('Which is more offensive? Book burning or flag burning?', function(res, convo) {
                convo.next()

                convo.ask('How often do you keep your promises?', function(res, convo) {
                    convo.next()

                    convo.ask('Which is bigger? The Sun or the Earth?', function(res, convo) {

                        convo.say('Thank you, that is all for now')
                        convo.next()

                    })
                })
            })
        })
    })


- For calling the method controller.spawn, you have to provide the token.




